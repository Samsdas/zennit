from .canonizers import SumCanonizer
import torchvision
from .composites import EpsilonGammaBox
from .attribution import Gradient
from .torchvision import ResNetBasicBlockCanonizer, ResNetBottleneckCanonizer
import inspect
import numpy as np

import torch

model = torchvision.models.resnet50()
# canonizers = [SumCanonizer()]
random_input = torch.randn(1, 3, 224, 224)
# EpsilonGammaBox needs keyword arguments 'low' and 'high'
composite = EpsilonGammaBox(low=-3., high=3., canonizers=[SumCanonizer()])

composite_bottleneck_canonizer = EpsilonGammaBox(low=-3., high=3., canonizers=[ResNetBottleneckCanonizer()])
composite_sum_canonizer = EpsilonGammaBox(low=-3., high=3., canonizers=[SumCanonizer()])
composite_no_canonizer = EpsilonGammaBox(low=-3., high=3., canonizers=[])

# composite_sum_canonizer = EpsilonGammaBox(low=-3., high=3., canonizers=[])

with Gradient(model, composite_bottleneck_canonizer) as attributor:
    # gradient/ relevance wrt. output/class 0
    # torchvision.vgg16 has 1000 output classes by default
    output, relevance_zennit = attributor(random_input, torch.eye(1000)[[0]])

with Gradient(model, composite_sum_canonizer) as attributor:
    # gradient/ relevance wrt. output/class 0
    # torchvision.vgg16 has 1000 output classes by default
    output, relevance_parsing = attributor(random_input, torch.eye(1000)[[0]])

with Gradient(model, composite_no_canonizer) as attributor:
    # gradient/ relevance wrt. output/class 0
    # torchvision.vgg16 has 1000 output classes by default
    output, relevance_nothing = attributor(random_input, torch.eye(1000)[[0]])

equal = np.array_equal(
    relevance_zennit.numpy(),
    relevance_parsing.numpy()
)

print(np.array_equal(
    relevance_zennit.numpy(),
    relevance_nothing.numpy()
))

print(equal)