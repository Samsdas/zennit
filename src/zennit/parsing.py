import inspect
import ast
import textwrap

import torch.nn as nn


class Parser:
    def __init__(self, module: nn.Module):
        self.module = module
        self.parse_forward_function()

    def parse_forward_function(self):
        forward_code = textwrap.dedent(inspect.getsource(self.module.forward))
        self.tree = ast.parse(forward_code)

    def change_forward_function(self):
        # Using the Transformer class, to visit all nodes in the abstract syntax tree, and if the node is "AugAssign (+=) then the node will changed into two nodes, with the correct statements
        Trasformer().visit(self.tree)
        # delivers the needed information of the new nodes, after the change
        tree = ast.fix_missing_locations(self.tree)
        # print(ast.dump(tree, indent=2))

        # Here the changed AST is compiled into string
        forward_code = ast.unparse(tree)

        return forward_code

    def check_for_addition(self):
        addition_existent = False
        for node in ast.walk(self.tree):
            if isinstance(node, ast.Add) or isinstance(node, ast.AugAssign):
                addition_existent = True
                break
        return addition_existent


class Trasformer(ast.NodeTransformer):

    def visit_AugAssign(self, node):
        return (ast.Assign(targets=[ast.Name(id='out', ctx=ast.Store())], value=ast.Call(
            func=ast.Attribute(value=ast.Name(id='torch', ctx=ast.Load()), attr='stack', ctx=ast.Load()), args=[
                ast.List(elts=[ast.Name(id='identity', ctx=ast.Load()), ast.Name(id='out', ctx=ast.Load())],
                         ctx=ast.Load())],
            keywords=[ast.keyword(arg='dim', value=ast.UnaryOp(op=ast.USub(), operand=ast.Constant(value=1)))])),
                ast.Assign(targets=[ast.Name(id='out', ctx=ast.Store())], value=ast.Call(
                    func=ast.Attribute(value=ast.Name(id='self', ctx=ast.Load()), attr='canonizer_sum', ctx=ast.Load()),
                    args=[ast.Name(id='out', ctx=ast.Load())], keywords=[])))
